package com.palindrom.palindromesatu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class PalindromesatuApplication {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the string:");
		String s = scanner.nextLine();
		System.out.println("Enter the value of k:");
		int k = scanner.nextInt();

		char[] str = s.toCharArray();
		int[] changes = new int[str.length];

		boolean isPossible = makePalindrome(str, changes, k, 0, str.length - 1);

		if (isPossible) {
			maximizePalindrome(str, changes, k, 0, str.length - 1);
			System.out.println(new String(str));
		} else {
			System.out.println("-1");
		}
	}

	private static boolean makePalindrome(char[] s, int[] changes, int k, int left, int right) {
		if (left >= right) {
			return k >= 0;
		}

		if (s[left] != s[right]) {
			if (k <= 0) {
				return false;
			}

			char maxChar = (char) Math.max(s[left], s[right]);
			s[left] = maxChar;
			s[right] = maxChar;
			changes[left] = 1;
			return makePalindrome(s, changes, k - 1, left + 1, right - 1);
		} else {
			return makePalindrome(s, changes, k, left + 1, right - 1);
		}
	}

	private static void maximizePalindrome(char[] s, int[] changes, int k, int left, int right) {
		if (left >= right) {
			if (left == right && k > 0) {
				s[left] = '9';
			}
			return;
		}

		if (s[left] != '9') {
			if (changes[left] == 1 && k >= 1) {
				s[left] = '9';
				s[right] = '9';
				maximizePalindrome(s, changes, k - 1, left + 1, right - 1);
			} else if (changes[left] == 0 && k >= 2) {
				s[left] = '9';
				s[right] = '9';
				maximizePalindrome(s, changes, k - 2, left + 1, right - 1);
			} else {
				maximizePalindrome(s, changes, k, left + 1, right - 1);
			}
		} else {
			maximizePalindrome(s, changes, k, left + 1, right - 1);
		}
	}
}
