# Balanced Bracket soal nomor 1

## Deskripsi
Program ini menentukan apakah string yang berisi tanda kurung `(`, `)`, `{`, `}`, `[`, `]` memiliki tanda kurung yang seimbang. Tanda kurung seimbang jika setiap tanda kurung buka memiliki pasangan tutup yang sesuai dalam urutan yang benar.

## Contoh
### Sampel 1
- **Input:** `([{}])`
- **Output:** `YES`

### Sampel 2
- **Input:** `([{]})`
- **Output:** `NO`

### Sampel 3
- **Input:** `({[]})`
- **Output:** `YES`

## Aturan
1. Tanda kurung yang diperbolehkan adalah: `(`, `)`, `{`, `}`, `[`, `]`.
2. Tanda kurung bisa dipisahkan dengan atau tanpa whitespace.
3. Periksa tanda kurung yang memiliki kecocokan antara kurung buka dan kurung tutup dengan mengembalikan nilai string `YES` atau `NO`.

## Kompleksitas
### Waktu (Time Complexity)
di mana n adalah panjang string. Setiap karakter dalam string diperiksa sekali.

### Ruang (Space Complexity)
semua karakter dalam string bisa dimasukkan ke stack jika mereka semua adalah tanda kurung buka.

## Penggunaan
Jalankan program dan masukkan string tanda kurung saat diminta. Program akan mencetak `YES` jika string seimbang, dan `NO` jika tidak.

## Contoh Penggunaan
```bash
$ java BalancedBracket
Input: ([{}])
Output: YES

$ java BalancedBracket
Input: ([{]})
Output: NO
 