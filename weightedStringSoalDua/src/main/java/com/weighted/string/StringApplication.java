package com.weighted.string;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

@SpringBootApplication
public class StringApplication {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Masukkan string: ");
		String s = scanner.nextLine();
		System.out.print("Masukkan queries (pisahkan dengan spasi): ");
		String[] queriesStr = scanner.nextLine().split(" ");
		int[] queries = new int[queriesStr.length];
		for (int i = 0; i < queriesStr.length; i++) {
			queries[i] = Integer.parseInt(queriesStr[i]);
		}
		List<String> result = weightedStrings(s, queries);
		System.out.println("Output: " + result);
	}

	public static List<String> weightedStrings(String s, int[] queries) {
		List<String> result = new ArrayList<>();
		Set<Integer> weights = calculateWeights(s);
		for (int query : queries) {
			if (weights.contains(query)) {
				result.add("Yes");
			} else {
				result.add("No");
			}
		}
		return result;
	}

	public static Set<Integer> calculateWeights(String s) {
		Set<Integer> weights = new HashSet<>();
		int currentWeight = 0;
		char prevChar = '\0';
		for (char c : s.toCharArray()) {
			if (c == prevChar) {
				currentWeight += c - 'a' + 1;
			} else {
				currentWeight = c - 'a' + 1;
			}
			weights.add(currentWeight);
			prevChar = c;
		}
		return weights;
	}
}
