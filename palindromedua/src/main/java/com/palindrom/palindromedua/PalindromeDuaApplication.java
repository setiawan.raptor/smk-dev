package com.palindrom.palindromedua;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class PalindromeDuaApplication {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the string: ");
		String str = scanner.next();
		System.out.println("Enter the number of replacements (k): ");
		int k = scanner.nextInt();
		String result = findHighestPalindrome(str.toCharArray(), k, 0, str.length() - 1, new boolean[str.length()]);
		System.out.println("Highest Palindrome: " + result);
	}

	public static String findHighestPalindrome(char[] arr, int k, int left, int right, boolean[] changed) {
		// Base case: if we have processed all pairs
		if (left > right) {
			return new String(arr);
		}

		// If we are out of changes and the string is not yet a palindrome
		if (k < 0) {
			return "-1";
		}

		// Process the current pair
		if (arr[left] != arr[right]) {
			// Change the smaller value to the larger one
			if (arr[left] > arr[right]) {
				arr[right] = arr[left];
			} else {
				arr[left] = arr[right];
			}
			changed[left] = true;
			k--;
		}

		// Recur for the next pair
		String result = findHighestPalindrome(arr, k, left + 1, right - 1, changed);
		if (result.equals("-1")) {
			return "-1";
		}

		// Try to maximize the palindrome if changes are left
		if (left == right && k > 0) { // Handle the middle character in odd length strings
			arr[left] = '9';
		} else if (k > 0 && arr[left] != '9') {
			if (changed[left]) {
				arr[left] = arr[right] = '9';
				result = findHighestPalindrome(arr, k - 1, left + 1, right - 1, changed);
			} else if (k >= 2) {
				arr[left] = arr[right] = '9';
				result = findHighestPalindrome(arr, k - 2, left + 1, right - 1, changed);
			}
		}

		return result;
	}
}
